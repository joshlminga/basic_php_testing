<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>First Page</title>
</head>

<body>

	<!--- Create a html link to another page: which will be second.php -->

Normal HTML link :	<a href = "secondpage.php"> Second Page </a> 

<br />
	<!--- that is normal html link; but we can add link id on each link using php

	* let say you want the same link to provide different response; if the use is not log in then log in if is login the log out...
	* you can even use a loop to loop through the link and assigned different id each time 
	* lets look at that and see..
-->

PHP link using IDs : <?php $link_name = "Second Page"; 
							$ids = 2;
						   if ($ids == 2) {

						   	$id = 2 ;
						   }
						   else { $id = 0;}

					 ?>


					 <a href = "secondpage.php?id=<?php echo $id; ?>"><?php echo $link_name; ?> </a> 

					 <?php // GO to using_getvalue.php ?>

<!-- Skip the below code and proceide to using_getvalue.php -->

<br />

					<?php $cartegory_name = "Adminstartor"; $page_id = 25 ; //consider review secondpage.php ?> 

PHP multiple IDs : <a href="secondpage.php?cartegory=<?php echo $cartegory_name; ?>&page=<?php echo "{$page_id}";?>"> Second Page </a>

<br />

<!-- Encoding Get values -->

<?php

	//let say we want to send informantion i.e company name = Josh & Sharon 

$company = "Josh & Sharon Ltd"; ?>

Get value No-Encoding : <a href="secondpage.php?company=<?php echo $company; ?>&page=<?php echo "{$page_id}";?>"> Second Page </a>

<!--- It will send the details through url correct but watch what happens on out put
	Company Name : Josh
	page : 25

	// to get the detail correct we must encode our value

-->
<br />

Get value Encoding : <a href="secondpage.php?company=<?php echo urlencode($company); ?>&page=<?php echo "{$page_id}";?>"> Second Page </a>
<!--- we have just add echo urlencode($compnay); // remeber to use echo... -->


<!--- if you inspect the urls you will see one is
	http://localhost/PHP_TESTING/secondpage.php?company=Josh%20&%20Sharon%20Ltd&page=25
															   ^
															   |
				   		It use characte %20&%20 : hence php take it as end of id value %20 == space 	

	http://localhost/PHP_TESTING/secondpage.php?company=Josh+%26+Sharon+Ltd&page=25
															  ^
															  |
										 It use +%26+ : letter php decode it automatically to original value + == space

	* This technique works only for $_GET values *
-->

<br />

Get value Raw Encoding : <a href="secondpage.php?company=<?php echo rawurlencode($company); ?>&page=<?php echo "{$page_id}";?>"> Second Page </a>

</body>
</html>

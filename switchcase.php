<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Switch Statement</title>
</head>

<body>
	<!--- You might use if ___ else statements more often than switch case;
	*But switch are more useful when we have a list of option and we know one of them is going to be used
	*e.g. Which way customer want to be contacted a : by email; b : by phone ; c : Post Address; 
	* when is option a - execute this block of code
	* Then we will have our default case: where non of the option is selected do what
-->

<?php 

$name = "Deno";

	switch ($name) {
		case 'Sharon':
			echo "Sharon Achieng";
			break;

		case 'Kwame':
			echo "Kwame Asiago";
			break;

		case 'Deno':
			echo "Dennis Shiraho";
			break;

		default:
			echo "No name selected";
			break;
	}
?>
	<br />

<!--- Switch case can be perfomed with numbers also -->

<?php 

$name = 1;

	switch ($name) {
		case 0:
			echo "Sharon Achieng";
			break;

		case 1:
			echo "Kwame Asiago";
			break;

		case 2:
			echo "Dennis Shiraho";
			break;

		default:
			echo "No name selected";
			break;
	}
?> 

<br />

<!--- Switch case can be performed using variables : Just look this example -->

<?php 

$year = 2014;

	switch (($year - 4) % 12) {
		case 0:	$zodiac = "Rat"; break;
		case 1:	$zodiac = "Mamba";	break;
		case 2: $zodiac = "Penguin"; break;
		case 3: $zodiac = "Scopion"; break;
		case 4: $zodiac = "Mushroom"; break;
		case 5: $zodiac = "Shark"; break;
		case 6: $zodiac = "Tiger"; break;
		case 7: $zodiac = "Jaguar"; break;
		case 8: $zodiac = "Human"; break;
		case 9: $zodiac = "Whale"; break;
		case 10: $zodiac = "Sunny"; break;
		case 11: $zodiac = "Jelly Fish"; break;
		case 12: $zodiac = "Duma"; 	break;

		default: echo "This is empty Year"; break;

	}

	echo "This is Year {$year} which is Zodiac Year for {$zodiac}" ;
?>

<br />
<!--- You can write your switch statement in one line and echo the variables outside the switch cae control and it will still work -->

<!--- Let's say you have three uses and you want to give them different response when the login -->

<?php

$User_type = "customer";
	switch ($User_type) {

		case 'user':
			echo "How is your day" ;
			break;
		
		case 'admin':
			echo "Login Sucessful" ;
			break;

		case 'customer':
			echo "Hellow welcome again" ;
			break;
				
		default:
			# code...
			break;
	}

	//we can switch to a group of code depends on user response

?> 

<br >

<!--- But what if we want to give the same response to certain two type of users e.g. User and Customer should get same message -->

<?php

$User_type = "user";
	switch ($User_type) {

		case 'user':        //  case for user is joined with
		case 'customer' :     // case for customer
			echo "How is your day" ;
			break;
		
		case 'admin':
			echo "Login Sucessful" ;
			break;

		
		default:
			# code...
			break;
	}

	//we can use two switch to one group of code

?> 

</body>
</html>

<?php

//Values
$investment = 1000;
$year = 0.083;
$rate = 10;
$n = 4;

	function comp_int($investment,$year,$rate,$n){

	    $accumulated=0;
		if ($year > 1){

		    $accumulated = interest($investment,$year-1,$rate,$n);
		}

	    $accumulated += $investment;
	    $accumulated = $accumulated * pow(1 + $rate/(100 * $n),$n);
	    
	    return $accumulated;
	}

$int = comp_int($investment,$year,$rate,$n);

echo "Interest : {$int}";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Continue</title>
</head>

<body>
	<!--- Is a php  operator used to skip the execution to the next oparation 
	*e.g.  is like a Hollywood director shout "NEXT" in the middle of audition
	* means okay we have seen enough of you let's go to next
-->

<?php
//let's see how it works: consider the last for loop example


for ($num=0; $num <= 10; $num++)
 { 
	
	if ($num == 5) {		
		
		Continue; // means if count == 5 : leave and go to next operation
	}

		echo $num. " , " ; // Lets out put the values and see what happened 

		//ooops! it count 1,2,3,4,6,7,8,9,10  | note : it skiped 5

}

?>


<br />
<!--- Let's look at the bigger picture: You are told to pick only odds number from 1-10... how can you do it?? -->
<?php
//consider the for loop and continue structure: we tell apache loop form 1 - 10 and skip value which when divided by 2 the reminder is 0 ($num % 2 = 0)

for ($num=1; $num <= 10; $num++) { 	if ($num % 2 == 0) {	Continue; } // we know even number when dived by 2 reminder is 0; so we skip all even numbers

		echo $num ." , " ; // only odds number will be outputed

		//1,3,5,7,9

}

?>

<br />

<!--- Using the while loop To show even Numbers-->
<?php

$count = 1 ;

while ($count <= 10) //condition to check
 { 
	
	if ($count % 2 != 0) { 		
		
		$count ++; // not we increment here because : 
					/* 
					* - we would stack in infinite loop because continue does not allow code block contain count ++ below to be reached
					* - so we will loop 1 forever... just look and see
					*/
		Continue; // we say "hey if you divide count by 2 and answer is not equal to 0 ;skip"

	}

	echo $count . "," ; // otherwise show integers only	
	$count ++;  //increment the value so we may not get inifnity loop (loop with no end)
}

?>

<br />

<!--- Loop inside a loop with continue... now show only odd numbers-->

<?php

for ($i=0; $i <= 5; $i++) { 
	if ($i % 2 == 0) { Continue; }  // we say hey if is even number skip

	for ($k=0; $k <= 5; $k++) { 

		if ($k == 3) { Continue(2); } // hey if you find 3 skip { continue has a default value of 1 (continue (1)) - so if we add something like continue(2)  }
									/*
									* We say continue , but loop from two forloops
									*/
		echo $i  ."-" . $k . "<br />";

		}


}

?>

</body>
</html>

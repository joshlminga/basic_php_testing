<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FUnction : Define</title>
</head>

<body>
<?php

// Function are used do define group of code that you see they might be used over and over 
/*
* e.g. we want to perform addition each time user open a page : instead of doing addition each time we can make one function to perform addition each time
*/ 

	function hello() {
		//function name can start with underscore ( _hellow) capital letter Hellow or numbers Hellow3

 		echo "Hellow! Raymond <br />"; // function wont be dispayed or performed till it is called
 	}

//how to call the function??

 	hello();  // we call the function using function name and (arguments)
?>

<!--- Now let's define function with arguments -->

<?php

function Say_Hellow($word) // not we can create a functin with a variable that accepts a certain input form user...
{
	echo "Hello to {$word}"; // we can the exploit that variable within the function 
}


Say_Hellow("Sharon"); // our user input word sharon to variable word ..

					/* consider this
					* $word = mysql_escape_string($POST("name")); // user input from form ... and let say user input word Sharon
					* so it will be
					* $word = "Sharon";
					* Back to our function it will be ...
					* Say_Hellow($word) ; //which is identical with saying 
					* Say_Hellow("Sharon");
					*/
?>

<!--- Note : In PHP 5.4 - 5.5 
	* We can call the function and define it later .... take a look at this example
<?php

//	Say_Hellow("Sharon"); 


//	function Say_Hellow($word) // not we can create a functin with a variable that accepts a certain input form user...
{
//	echo "Hello to {$word}"; // we can the exploit that variable within the function 
}

// ------- Note: we call the function first and the we define it

?>

-->

<?php // However once a Function is defined we can't redefine it again
	// Function can be defined inside a IF statement, Loop switch and other Logical operators.

 ?>



</body>
</html

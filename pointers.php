<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pointers</title>
</head>

<body>

	<!--- In array php use pointer to know which variable you are current working with :
	* consider a for loop example : It has a power to move pointer to the next value [$count ++;] 
	* but not only for loop has that power to move pointer; 
	* We can use pointe to know the current iteam we use, the previous and next item 
-->

<?php

$numbers = array(8,23,4,16,7,9);

//lets first see the whole array

echo "<pre>" ;

print_r($numbers) ;

echo "</pre>" ;

echo "<br />";

//let us current function to get the current value the pointer is pointing at

echo "First Value : " . current($numbers) . "<br />";

// we can use next to get the next iteam pointer will move at after done with the first one

//next($numbers);
echo "Second Value :" . next($numbers) . "<br />";

// Now lets skip the third value and look what will pointer point as the fourth value

next($numbers);  // 3rd value
next($numbers);  // 4th value :: we skiped the 3rd one by say next then next
echo "Fouth Value : " .current($numbers) . "<br />";

// We can go back to second value and look at it again

prev($numbers); // Go back from 4th - 3rd  
prev($numbers); // Go back from 3rd - 2rd
echo "Previous on Second : " .current($numbers) . "<br />";  

/*
* What if we want to move our pointer back to first position or the last
*/

reset($numbers); // to reset and put the pointer to the first value
echo "Pointer Reset : " . current($numbers) . "<br />";

end($numbers); // to put the pointer to the last value
echo "Pointer to Last Value : " .current($numbers) . "<br />";

next($numbers); // lets see what it will show if we look at the next value after we read the last data
echo "Next Value after the END : " . current($numbers) . "<br />" ; // we will get nothing (empty)

?>
<br />

<!--- now let's see how this could be done in while loop -->
<?php
	
	//first let's reset the pointer because current it's point at position 6 which is empty *remember at line 61 - 62 above
	reset($numbers);

	//same as foreach loop

	while ($numb = current($numbers)) {
		echo "{$numb} <br />" ;
		next($numbers); // we say okay move to next value 
	}

?>

</body>
</html>

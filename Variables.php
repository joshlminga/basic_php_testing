<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Variables</title>
</head>

<body>


<?php

	/*STRING TO VARIABLE*/

	$str = "first=value&arr[]=foo+bar&arr[]=baz";
	parse_str($str);
	echo $first."<br />";  // value
	echo $arr[0]."<br />"; // foo bar
	echo $arr[1]."<br />"; // baz

	echo "*************************************************** <br />";

	parse_str($str, $output);
	echo $output['first']."<br />";  // value
	echo $output['arr'][0]."<br />"; // foo bar
	echo $output['arr'][1]."<br />"; // baz



	//Value
	$form_data = array('paid' =>1000, 'balance'=>200,'received'=>'19-08-2016', 'status' =>'Paid', 'method'=>'Mpesa');

	//Array
	foreach ($form_data as $key => $value) {

		//Text To Variable
		$$key = $value;
	}

	echo "*************************************************** <br />";

	echo $paid."<br />";
	echo $balance."<br />";
	echo $received."<br />";
	echo $status."<br />";
	echo $method."<br />";

?>


</body>
</html>

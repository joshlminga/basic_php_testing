<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Function : Default Argument</title>
</head>

<body>

	<!--- We all know if we declaire a function with two arguments; then we must call the function with two arguments
	*  But there is a way to do so 
	  -->
<?php

//lets see..

function paint($colors) {
		
 		echo "The colour of sky is : {$colors} <br />"; 
 	}



 	echo paint("blue");  // but if we remove the argument "blue" : we will get an error....
 		/*
 		* but lets say if there is no input what should be the default value??
 		* Let's set one
 		*/

?>
<br />

<?php
function names($colors = "Josh Minga") {  // so we say if there is no argument received, then use the default value
		
 		echo "Your name is : {$colors} <br />"; 
 	}



 	echo names();  // there is no input here....
 		
?>

<br />
<!--- Let's pass two arguments and sent four option one with no arguments, another with, one with null value iside and while another not complete -->

<?php

function task($time = "morning" , $action = "Brash your teeth")   //default values
	{	

 		echo "In {$time} you should {$action} . <br />"; 
 	}



 	echo task();  // there is no input here.... show default value
 	echo task("afternoon", "study"); // we set our custom /user arguments
 	echo task("evening");  //only one arguments
 	echo task("summer", null); // one is null

 	// note : on null argument apache did not pick the default of variable action why?? isn't it null??
 	// the reason is becaul in some cases e.g. null is not empty ... but empty is null..


// The most needed arguments should be listed first while the least needed can follow

?>
</body>
</html>

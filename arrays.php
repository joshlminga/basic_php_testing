<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Arrays</title>
</head>

<body>
<?php

/*
* We use array to store number of items
* For example we have 100 customers, instead of use 100 variable we can use 1 array
* We can store 100 customers email in 1 array and retrive those emails using array index number
* In array we use array name and index number; "say 'give me whats is in box in position 3' "
*/

$numbers = array(); //we have create an empty array and assign to variable numbers
$numbers1 = array(4,5,16,30,22,55,3); //we have create an assigned integer array (not empty array)  and assign to variable number1 
$mixed = array(44,"James","Fox","Dog",3.14); //array can contain mixed data types
$more = array("Sharon",45,"Deno", array("Justine",101,"Kwame")); // we can declare array within an array 
?>

<br />
View 1st item in array numbers1 : <?php echo $numbers1[0]; //we use[] brackets to print an array :note 0 = 1st object in an array:?> <br />
Viewing mixed array : <?php echo $mixed[3]; //we will get word Dog and not Fox; number inside [] indicate array position: 0,1,2 ...?> <br />

<br />
<pre>
Preview whole array : <?php print_r($mixed);//print_readable(print_r);we use this method to view what is in array not for user but for us ?> <br />
</pre>

<br />
View Array in Array : <?php echo $more[2]; // we know what is in position 2 in array more (Deno) ?> <br />
<pre>
Array in Array : <?php print_r($more); // lets see whats is in position what in which array ?><br />
</pre>

Viewing Kwame in array more :<pre> <?php echo $more[3][2]; //we take the position which our inside array is and the value we want inside it ?> </pre><br />

<br />
Adding Value to an array 1 : <?php $more[1] = 20; echo $more[1]; // we want to put 20 on array more in potion 1 which was 42 ?> <br />
Adding value to inside array 3 - 0 : <?php $more[3][0] = "Asiago" ; echo $more[3][0]; //we change Justine to Asiago in array more position 3 inside position 0 ?> <br />
Assign Array 4 : <?php $more[4] = "Josh"; echo $more[4]; // there is no position 4; Yes but we can assaign value to it and it will be created ?> <br />
Array inside 3 - 3 : <?php $more[3][3] = 3.45 ; echo $more[3][3]; // new value in inside array ?> <br />
Assign to a blank position : <?php $more[] = "Emanuel"; echo "Emanuel";//assign to the first empty position  ?> <br /> 

<pre>
See the whole array again : <?php print_r($more); //we can see the chages here ?> <br />
</pre>
</body>
</html>

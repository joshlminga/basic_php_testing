<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Function : Arguments</title>
</head>

<body>
	<!--- Remember the synatx of function ??

	function name($argument1, $argument2) {
	
		statement;

	} 

-->

<?php
// we have already work with function that has one argument...

function Say_Hellow($word) // not we can create a functin with a variable that accepts a certain input form user...
{
	echo "Hello to {$word} <br />"; // we can the exploit that variable within the function 
}


Say_Hellow("Sharon"); // our user input word sharon to variable word ..

// Now let's look function with two arguments
?>

<br />

<?php
// Consider my bottom line instractions in function_define.php...

function SayHellow($instact) 
{
	echo "{$instact} <br />"; 
}

$instact = "Hellow Sharon, are you fine? I miss you.";

SayHellow($instact); // we automatically say! we want $instact to be manupulated by function SayHellow

// Now You see what I meant when I say we accept input and store it in variable the manupulate it using the function !!

?>

<br />

<!--- Consider the next example, I will use different varibale names and out put same result -->

<?php
// Consider my bottom line instractions in function_definition.php...

function Union1($message) //It doesn't matter if the variable names are different
{
	echo "Dear Sharon, <br />{$message} Yours in study <i>John Musil</i> <br />"; 
}

$letter = "We welcome you to the <i>Pens down Bashment Part </i> which will take place at- <br /> Masai Mara National Park; our main host will be Mr LION. <br />";

Union1($letter); // Look we use variable letters as primary input from user: but we did not use the same variable name in the function

?>

<br />

<!---
	Consider the following example: 
	* we used | str_replace("Atwoki", "Josh", $second); | to replace the word Atwoki with Josh : now  that is function but was predefine by PHP
	* if we were to define one of our own we would say:
	* function str_replace("Atwoki", "Josh", $second) { statement; } // so that is the relationship in these two
-->

<?php
// Now if we define function with three arguments then we should call with three argument inside: check this

$name = "Kwame Asiago";

function better_hellow($Greetings, $Comments, $Choice) 
{
	echo $Greetings ." ". $Comments ." ". $Choice;
}


better_hellow("Hellow", $name, "!! <br />"); // I manitain three arguments
better_hellow("Greetings", $name, null); echo "<br />"; // note: I use null on the third argument, (because I have nothing to put there) and apache took that
														// There has to be something to complete all 3 arguments. 

?>



</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Second Page</title>
</head>

<body>

<!-- from using_getvalue.php 
	* Now let's print_r our associative array here -->

	<pre>

		<?php print_r($_GET);  // let's print our get value ?>

<!--- You will get :  
		
		Array
(
    [id] => 2
)

--> 
<?php // if happens you get empty array just go back to firstpage.php and using the second link PHP link using IDs :
	  // or paste this [ secondpage.php?id=2 ] |to see the out put ?>

<!-- lets go back to firstpage.php and add anothe value and see the out put -->
<!-- You get 

	Array
(
    [cartegory] => adminstartor
    [page] => 25
)

- That means each time a different request sent; different response will take place

-->

	</pre>

<br />

<!--- That's is cool isn't it?? but we have just echo the value to the browser!! what if we want to use them??
	* instead of using print_r ... let's exploit the value sent from firstpage.php
-->

<?php

 

 	if (isset($_GET['cartegory']) && !empty($_GET['cartegory'])) {

 	 $cartegory = $_GET['cartegory'];  // we declaire avariable (any variable name) the we use super global $_GET and ['request_id_name']; to get the actual value sent

 		echo "Cartegory Name : {$cartegory}";
  	}
  	elseif (isset($_GET['id']) && !empty($_GET['id'])) {
  		
  		 	$id = $_GET['id']; 


  		echo "Account ID : {$id} "; 
  	}
    elseif (isset($_GET['company']) && !empty($_GET['company'])) {
      
      $company_name = $_GET['company'];
      echo "Company Name : {$company_name} <br />";

      if (isset($_GET['page']) && !empty($_GET['page'])) {
         $page = $_GET['page']; 
      echo "Page ID : {$page} "; 
      }
      
    }
  	else {

  		echo "No Selection";
  	}

  	/* Since there is different response in each link; to avoid error we should first theck if the value is set and is not empty 
  	* Go to your web browser; access firstpage.php and click each link to see the output
  	*/

  	// MAIN syntax - - -- - 
  	/*
  			$id = $_GET['id'];       // take the super global $_GET [with the requered id name] and assign to the variable
  		echo "Account ID : {$id} ";  // now the variable is set and can be used any where..
  	*/

  	?>

</body>
</html>

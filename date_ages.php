<?php 

date_default_timezone_set('Africa/Nairobi');

$then = new DateTime('2018-04-30 18:41:22');
$now = new DateTime();
$delta = $now->diff($then);

$quantities = array(
    'year' => $delta->y,
    'month' => $delta->m,
    'day' => $delta->d,
    'hour' => $delta->h,
    'minute' => $delta->i,
    'second' => $delta->s);

$str = '';
foreach($quantities as $unit => $value) {
    if($value == 0) continue;
    $str .= $value . ' ' . $unit;
    if($value != 1) {
        $str .= 's';
    }
    $str .=  ', ';
}
$str = $str == '' ? 'a moment ' : substr($str, 0, -2);

echo $str;

echo "<br>";

$delta = date_diff($then,$now);
echo $delta->format("%a");

echo "<br>";

$to_time = strtotime("2018-05-08 17:01:22");
$from_time = strtotime(date('Y-m-d, H:i:s')); //"2018-04-30 18:41:40");

echo round(abs($to_time - $from_time) / 60,2). " minute";

echo "<br>";

echo date('Y-m-d, H:i:s');

echo "<br>";

$timeFirst  = strtotime('2018-05-08 14:11:22');
$timeSecond =  strtotime(date('Y-m-d, H:i:s'));
$differenceInSeconds = $timeFirst - $timeSecond;

echo $differenceInSeconds;

echo "<br>";
$date = date_create('2018-05-15 17:11:22');
$newDate = date_format($date, 'Y-m-d H:i:s');


$expire = date('Y-m-d H:i:s', strtotime("+7 days", strtotime('2018-05-15 17:11:22')));

// $expire =  date($newDate, strtotime("+7 days"));

echo "$expire";

?>
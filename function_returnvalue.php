<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Function : Return Value</title>
</head>

<body>
	<?php
		//let say we want a function not to echo value to the user but to accept the input, perform the operation and return the value for futer use

// now how to get the value outside the function??
/*
* we use returm $value;
* Then we catch that value ouside the function using another variable.
*/

function add_input($val1, $val2) {

	$sum = $val1 + $val2 ; // we accept two argument... add them and assign answer to another variable : so we may manupulate $sum the way we want
	return $sum; // we return the value back to us....
}

add_input(3,4); // we called the function and tell we have two arguments 3 and 4 now add them

// echo $sum;                 //this wont work, mean we can not get the variable outside the function this way.... Try to uncomment at the begining and see

$result = add_input(3,5); // we catch the returned value using variable result ... 
echo "{$result} <br />"; // now we can use varibale result to echo the answer outside the function

// And see the answer is 8 and not 7 as what our function input were at line 24, because returned the value to us and we process it using $result at line 28
// we can even use the returned value further... look below

$result1 = add_input(8,$result);
$answer = add_input(4,$result1); 

echo "{$answer} <br />";  // our answer will be 20; just look above code and remeber now $result = 8; because of affect at line 28.
?>

<!--- Now do you remember that example of Zodiac Years in switch.php... lets do that using function and see how return works as break in functions -->

<br />

<?php 

function chinise_zodiac($year) 
{

	switch (($year - 4) % 12)  // it does the same calculations
	{
		case 0:	return "Rat";
		case 1:	return "Mamba";	
		case 2: return "Penguin"; 
		case 3: return "Scopion"; 
		case 4: return "Mushroom"; 
		case 5: return "Shark"; 
		case 6: return "Tiger"; 
		case 7: return "Jaguar"; 
		case 8: return "Human"; 
		case 9: return "Whale"; 
		case 10: return "Sunny"; 
		case 11: return "Jelly Fish"; 
		case 12: return "Duma"; 	

		default: echo "This is empty Year"; 
	}
}

	// instead of useing break we return each value we catch as the wright one

$zodiac_year = chinise_zodiac(2014); // as usual we assign the value to be out-puted
echo "2013 is the year of : {$zodiac_year} <br />"; // we show the answer here 
echo "2027 is the year of : " . $zodiac_year = chinise_zodiac(2013); //note we have already echo at the beginning of line 73

?>

<!--- It is a good practise to echo the values outside the function -->


</body>
</html>

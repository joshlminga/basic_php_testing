<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Associative Array</title>
</head>

<body>
<?php
/*
* We use an associative array to store number of items just like in array
* How ever in associative array we use object name and not index number
* Is like say"Give me 100 costomers monthly order, "
* In associative array we do not know the index position of a value we!
* We use the Key and value identifier (name) to get the value we want
*/

$names = array(); // As it looks the method of declaration is simillar to normal array
$names1 = array("First_Name" => "Dennis", "Last_Name" => "Shiraho"); // This is how we assign value to associative array using Key-First_Name Value-Dennis

?>
<br />

First Name : <?php echo $names1["First_Name"]; // in array we used index number, but in associative array we use the key to output the value stored ?> <br />
Last Name : <?php echo $names1["Last_Name"]; // use the key to echo the value stored in associative array ?> <br />
Full Name : <?php echo $names1["First_Name"] ." ". $names1["Last_Name"]; // this is the best way to congatinate array which uses key ?> <br />
Full Name : <?php $names1["First_Name"] .= $names1["Last_Name"];  echo $names1["First_Name"]; //note there is no space between names ?> <br />

<pre>
Whole associative array : <?php print_r($names1); // echo whole associatve array ?> <br />
</pre>

<!--- Note there is no space between them: First Name and Last Name; if you try to space any character within those key you will get an error
	*remember in php space (" ") is considered as an iteam, same goes to number 0 they are not taken as empty
-->
<?php //$two = array('one' => '','two' => 32);?>

<br />

<!--- Assign value in associative array --> <?php $names1["First_Name"] = "Tabz" ; // it the same as in normal arrays ?><br />

Show Tabz : <?php echo $names1["First_Name"]; // echo the value we have just assigned ?> <br />

<pre>
Whole associative array : <?php print_r($names1); // echo whole associatve array ?> <br />
</pre>

<!--- Using normal array we wolud say $num = array(2,7,4); and we will echo $num[0]; to get value number 2; is the way as declaring an 
	associative array $num = array(0 => 2, 1 => 7, 2 => 4); and we would echo $nume[0]; to get value number 2 -->

<?php $num = array(0 => 2, 1 => 7, 2 => 4); //assagin value in an associative array ?><br />
View Key number 0 : <?php echo $num[0] ; // get what store in key 0 ?> <br />

<pre>
Whole associative array : <?php print_r($num); // echo whole associatve array ?> <br />
</pre>

<!-- we can also use any key e.g $num = array(0 => 2, 14 => 7, 22 => 4); there is key 0, 14 and 22 ; no more 0,1,2 ... -->

<?php $num = array(0 => 2, 14 => 7, 22 => 4); //assagin the key in numbers the way you want ?>

View Key number 14 : <?php echo $num[14] ; // get what store in key 14 ?> <br />

<pre>
Whole associative array : <?php print_r($num); // echo whole associatve array ?> <br />
</pre>

<?php $two = array('one' => 22); ?>
 
<!--<pre>
Two Crazy Associate : <?php //print_r($two); // echo whole associatve array ?> <br />
</pre> -->

<br>
<br>
<?php $estates = array('type'=>'estate', 'location'=>'Nairobi', 'more'=>[24,5,6]) ?>
Associative + Array : <?php var_dump($estates); ?> <br>


</body>
</html>

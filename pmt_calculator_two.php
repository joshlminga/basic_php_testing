<?php

    $mortgage = pmt(12, 2, 5000000); 
    echo "Your payment will be {$mortgage} ";
?>

<?php
/**
* PHP Version of PMT in Excel.
*
* @param float $apr
*   Interest rate.
* @param integer $term
*   Loan length in years.
* @param float $loan
*   The loan amount.
*
* @return float
*   The monthly mortgage amount.
*/
function pmt($apr, $term, $loan) {
  $term = $term * 12;
  $apr = $apr / 200;
  $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
  //$total_pay = $monthly_amt * 12;
  //$amount = $total_pay/$pay_sch;
  return number_format($amount, 2);
}
?>
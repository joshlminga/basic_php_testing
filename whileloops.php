<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>While Loop</title>
</head>

<body>
	<!--- Are use full when you want to repeat the same funtion when the condtion is true 
	*e.g. countinue to fill the bucket as long as bucket is not full
-->

<?php 
//Let's see how it's works 

$count = 0 ;

while ($count <= 10) //condition to check
 { 
	
	echo "Count = {$count}  <br />" ;
	$count ++;  //increment the value so we may not get inifnity loop (loop with no end)
}

echo "Finally Count ends as : {$count} <br />"; // after count = 11; the loop will check if 11 <= 10: which is False : then the loop will break and next line of code will be executed 

?>
<br />
<!--- Let's add some if__else condition in while loop: consider the last example -->

<?php 
//Let's see how it's works 

$count = 0 ;

while ($count <= 10) //condition to check
 { 
	
	if ($count == 5) {		
		
		echo "FIVE ," ; // when variable count is equal to 5 show string five instead of integer 5 
	}
	else {
		echo $count . "," ; // otherwise show integers only
	}
	
	$count ++;  //increment the value so we may not get inifnity loop (loop with no end)
}

echo "<br /> Finally Count ends as : {$count} <br />"; // after count = 11; the loop will check if 11 <= 10: which is False : then the loop will break and next line of code will be executed 

?>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Url Encode</title>
</head>

<body>

<?php

	$company = "John & Brothers";



	//For GET REguerst Only
	// secondpage.php?id=5&company=John&Brothers

?>

<a href="secondpage.php?id=5&company=<?php echo urlencode($company); ?>">Second Page</a>


<?php // There is also urldecode(str)  in case you need to use it ?>
<?php // There is also rawurlencode(str)  which encode string to %20 instead of + ?>

</body>
</html>

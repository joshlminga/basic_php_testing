<!-- Example client script for JQUERY:AJAX -> PHP:MYSQL example -->

<html>
  <head>
    <script language="javascript" type="text/javascript" src="jquery.js"></script>
  </head>
  <body>

  <!--  1) Create some html content that can be accessed by jquery -->
  <h2> Client example </h2>
  <h3>Output: </h3>
  <p id="output">this element will be accessed by jquery and this text replaced</p> <br />
  <div></div>

  <script id="source" language="javascript" type="text/javascript">

  $(function () 
  {
    //-----------------------------------------------------------------------
    // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
    //-----------------------------------------------------------------------
    $.ajax({                                      
      url: 'api.php',                  //the script to call to get data          
      data: "",                        //you can insert url argumnets here to pass to api.php
                                       //for example "id=5&parent=6"
      dataType: 'json',                //data format      
      success: function(data)          //on recieve of reply
      {

        //--------------------------------------------------------------------
        // 3) Update html content
        //--------------------------------------------------------------------

        /*
        data = [[{"seq":"1","sender":"2","recipient":"1","sms":"Hellow Achesa","time":null,"ref":"572778","flg":"1","stamp":"2016-10-19 16:07:15","remark":"AUTO"},{"seq":"2","sender":"1","recipient":"2","sms":"Hi it's Elvin","time":null,"ref":"572778","flg":"1","stamp":"2016-10-19 16:07:46","remark":"AUTO"},{"seq":"3","sender":"1","recipient":"2","sms":"how can I help you today","time":null,"ref":"572778","flg":"1","stamp":"2016-10-19 16:07:54","remark":"AUTO"},{"seq":"4","sender":"2","recipient":"1","sms":"am just doing follow ups","time":null,"ref":"572778","flg":"1","stamp":"2016-10-19 16:08:04","remark":"AUTO"}]];
        */

        //var count = Object.keys(data[0]).length;
        var count = Object.keys(data).length;

        var x = 0;

          $('#output').html("<b>all: </b>"+ JSON.stringify(data)); //Set output element html


          while (x <= count){

            // Create element with HTML
      //var txt = "<b> sms: "+ data[0][x].sms + "</b>" + "<i> from : " + data[0][x].sender + "</i>" + "<i> recepient :" + data[0][x].recipient+ " <br />";  
            var txt = "<b> sms: "+ data[x].name + "</b> <i> from: " +  data[x].ref + "</i>" + "<u> order: " + data[x].id + "</u> <br />";  

            $("div").after(txt); // Insert new elements after <img>
            x++;
          }

        //recommend reading up on jquery selectors they are awesome 
        // http://api.jquery.com/category/selectors/
      } 
    });
  }); 

  </script>
  </body>
</html>
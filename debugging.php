<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debugging</title>
</head>

<body>

	<!-- sometimes we may found our code does different from what we expect them to do: hence we will need to see what happened ??
	we can check over values by echo and see what is in the variable e.t.c
-->
<?php
//ways to see a bug in php
/*
*   echo $variable; // we can echo the variable to see what is in it
*   print_r ($array); // we can see what is in the array
*	gettype($variable); // we can see what is in the variable; is in a string as we expect it to be??
*	var_dump($variable); // Gives back each variable type, what is on it (int,string ...) and is of what size
*	get_defined_vars(); // return all array values used and those php use (not-user defined)
*	debug_backtrace(); // it will trace back your functions and you can see if there is an error
*/

// That's is enough to start with let's practise it

$number = 5;
$string = "Bug";
$array = array(0 => "Home", 1 => 3 , 2 => "Services" );

// we have already use gettype, print_r and echo... so let us move to the last three

?>
Var Dump 1 : <?php var_dump($number); // it told us that this is integer and the value stored is 5 ?>
		 2 : <?php var_dump($string); // it told us this is string and value stored is Bug  and it gives us it's lenght = 3?>
		 3 : <?php var_dump($array); // it tells us this is an array and of what size (element in that array) , it show on each key what is stored, is the value integer or sting and the length of the string stored ?>

<!--- when usong var_dump($variable); we did not use echo; because var_dump  already dump all values to the screen -->

<br />

<!--- when using get_defined_vars .. we have to echo all the values, since it is an array is best to use print_r -->

Get Defined Vars : <pre> <?php print_r(get_defined_vars($array)); // it returned array used on the file and our defined array can be seen at the bottom ?> 
				   </pre>  
				 
<!--- We have first to declaire a function then try to trace it back,
	* note : var_dump automatically dump all the values to the sceeen
	* and since am not working with arrays am not going to use print_r
	* neither echo because am not dealing with variables
	* the best way I will use var_dump in place of echo : because thi is a function
	* Look at the following example
-->

<?php

function Say_Hellow($word) 
{
	echo "Hello to {$word}";  // -- now lets say we have a promblem inside the function.... we can simply do

	var_dump(debug_backtrace()); /* It will tell us the file location
								  * The line 
								  * function name
								  * which argument is passed
								  * length of the values
								  */

}


Say_Hellow("Sharon"); 


// REMEMBER WE USE THESE DEBUG CODE TO FIND BUG AND SOLVE IT; ONCE WE ARE DONE WE HAVE TO REMOVE THESE CODES SO THAT THEY MAY NOT END UP IN OUR WEB PAGE

?>

<!--- PHP does not have a debug that can help you to debug your code;
	However there are online debugger that can help you so;

Xdebug
	- www.xdebug.org

DBG
	- www.php-debugger.com/dbg/


FirePHP (FireBug Series)

	- www.firephp.org

-->
</body>
</html>

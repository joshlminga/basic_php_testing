<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Booleans</title>
</head>

<body>
<?php 
 	// we declare two variable and assign boolean true and false on each
	$result1 = true ;
	$result2 = false ;
?>

<br />
<!--- Let's see our values stroed on each boolen variable -->
Result 1 : <?php echo $result1 ; // we get true as 1 ?> <br />
Result 2 : <?php echo $result2 ; // we get nothing as false ?> <br />  

<!--- We can check is the value is boolen (either True of False) -->

Is Result 2 Boolean ? : <?php echo is_bool($result2); // we get 1 (true) because boolean is true / false and $result2 = false ?> <br /> 

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Random Numbers</title>
</head>

<body>

<?php

	//No repeate
	function randomGen($min, $max, $quantity) {
	    $numbers = range($min, $max);
	    shuffle($numbers);
	    return array_slice($numbers, 0, $quantity);
	}

	$numbers1 = randomGen(0,999,2);
	print_r($numbers1); //generates 4 unique random numbers

	echo "<br>";

	$rand = implode("",$numbers1);

	echo $rand;

	echo "<br>";

	function randomGen2() {

		$min=0; $max=999; $quantity=2;

	    $numbers = range($min, $max);
	    shuffle($numbers);
	    $numbers1 = array_slice($numbers, 0, $quantity);

		$rand = implode("",$numbers1);

		return $rand;
	}

	echo "2: ".randomGen2();

?>


</body>
</html>

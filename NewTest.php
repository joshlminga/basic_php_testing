<!DOCTYPE html>
<html>
<head>
	<title>NEW TESTING</title>
</head>
<body>


<?php
function normal_bubble_Sort($my_array )
{
	do
	{
		$swapped = false;
		for( $i = 0, $c = count( $my_array ) - 1; $i < $c; $i++ )
		{
			if( $my_array[$i] > $my_array[$i + 1] )
			{
				list( $my_array[$i + 1], $my_array[$i] ) =
						array( $my_array[$i], $my_array[$i + 1] );
				$swapped = true;
			}
		}
	}
	while( $swapped );
return $my_array;
}

$original_array = array(3,2,1,4,7);
echo "Original Array :\n";
echo implode(', ',$original_array );
echo "<br>";
echo "\nSorted Array\n:";
print_r(normal_bubble_Sort($original_array));
?>

<br>
<br>

<?php

function enhanced_bubble_Sort($my_array,$original)
{
	do
	{
		$swapped = false;
		for( $i = 0, $c = count( $my_array ) - 1; $i < $c; $i++ )
		{
			if( $my_array[$i] > $my_array[$i + 1] )
			{
				list( $my_array[$i + 1], $my_array[$i] ) =
					array( $my_array[$i], $my_array[$i + 1] );
				$swapped = true;
			}
		}
	}
	while( $swapped );

	//Enhance
	for ($i=0; $i < count($my_array); $i++) { 
		
		$curr = $my_array[$i];
			
		for ($x=0; $x < count($original); $x++) { 
				
			if ($curr == $original[$x]) {
				
				$enhanced[$x] = $curr;
				
				break;
			}
			else{

				continue;
			}
		}
	}

return $enhanced;
}

$original_array = array(3,2,1,4,7);
echo "Original Array :\n";
echo implode(', ',$original_array );
echo "<br>";
echo "\nSorted Array\n:";
print_r(enhanced_bubble_Sort($original_array,$original_array));

?>


</body>
</html>
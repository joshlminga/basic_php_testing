<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Null and Empty</title>
</head>

<body>
<!--- in PHP we can declaire a variable and set it as null ; But how can we know the value given is null? -->

<?php 

$var1 = null ;
$var2 = "";
$var3 = " ";
$var4 = 0;
?>

<br />

<!--- now let's check if the values are null -->

Is Var1 Null : <?php echo is_null($var1); // is saying "is var1 a null value?" answer will be true (1) ?> <br />
Is Var2 Null : <?php echo is_null($var2); // is saying "is var2 a null value?" answer will be false (nothing) ?> <br />  
Is Var3 Null : <?php echo is_null($var3); // is saying "is var3 a null value?" answer will be false (nothing) ?> <br />  
Is Var4 Null : <?php echo is_null($var4); // is saying "is var4 a null value?" answer will be false (nothing) ?> <br />  

<!-- Now wait we did not assign any value in var2 and var 3; how comes it is not empty?? -->

<?php // Variable var2 is not empty because in PHP space or non-space(" ","") is considered as a value ?> <br />

<!-- Lets solve the argument and check if the values are set (means are they assigned?) -->

Var1 is set? : <?php echo isset($var1); // answer will be false(nothing) because there is no value set in variable var1 ?> <br />
Var2 is set? : <?php echo isset($var2); // answer will be true(1) because we set non-space value ("") in variable var2 ?> <br />
Var3 is set? : <?php echo isset($var3); // answer will be true(1) because we set space value (" ") in variable var3 ?> <br />

<!--- so avoid to use non-spaced or spaced value in the logic of thinking the apache will take it ase null value, no it is not null -->

<br />

<h2>Lets check Empty</h2>

<!--- in PHP value which considerd empty are (0 , 0.0 , "" , array() , empty , false , "0" , null) -->
<?php $var4 = 0; $var5 = array(); $var6 = "0"; // let's check these variable also if they are empty ?>

Is Var1 empty ? : <?php echo empty($var1); // is variable 1 empty? answer will be true(1) Yes it is empty ?><br />
Is Var2 empty ? : <?php echo empty($var2); // is variable 1 empty? answer will be true(1) Yes it is empty ?><br />

<!--- becareful though space is considered as null value it is not empty because space is a character: remember when countnig characters we also 
	count space e.g. Josh Minga : there are 10 characters in word Josh Minga || and not 9 characters -->
	
Is Var3 empty ? : <?php echo empty($var3); // is variable 1 empty? answer will be false(nothing) space is not empty ?><br />

<br />

Is Var4 empty ? : <?php echo empty($var4); // is variable 1 empty? answer will be true(1) Yes it is empty ?><br />
Is Var5 empty ? : <?php echo empty($var5); // is variable 1 empty? answer will be true(1) Yes it is empty ?><br />
Is Var6 empty ? : <?php echo empty($var6); // is variable 1 empty? answer will be true(1) Yes it is empty ?><br />


</body>
</html>

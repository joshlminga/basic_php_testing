<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reading Cookie</title>
</head>

<body>

<pre>
	<!-- Cookie is a Global variable hence we can access it's value by -->
	<?php print_r($_COOKIE);  ?>
</pre>

<?php

//Let use cookie

$test = $_COOKIE["test"];
echo $test;

echo "<br />";
//The most important this is to test if cookie exist before even start using them

if (isset($_COOKIE)) {
	$test = $_COOKIE["test"];
} else{
	$test = "";
}

echo $test;

echo "<br />";
//Other way to do better test is

$test = isset($_COOKIE["test"]) ? $_COOKIE["test"]: "";
echo $test;


//Unset Cookie
	$name = "test";
	$value = 123;

setcookie($test,null);

//OR

setcookie($name, $value, time() - 3600);

echo "<br />";
//Check Cookie Value Again

$test = $_COOKIE["test"];
echo "Check : " .$test;

?>

</body>
</html>

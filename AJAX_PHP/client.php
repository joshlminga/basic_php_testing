<!---------------------------------------------------------------------------
Example client script for JQUERY:AJAX -> PHP:MYSQL example
---------------------------------------------------------------------------->

<html>
  <head>
    <script language="javascript" type="text/javascript" src="jquery.js"></script>
  </head>
  <body>

  <!-------------------------------------------------------------------------
  1) Create some html content that can be accessed by jquery
  -------------------------------------------------------------------------->
  <h2> Client example </h2>
  <h3>Output: </h3>
  <p id="output">this element will be accessed by jquery and this text replaced</p> <br />
  <div></div>

  <script id="source" language="javascript" type="text/javascript">

  $(function () 
  {
    //-----------------------------------------------------------------------
    // 2) Send a http request with AJAX http://api.jquery.com/jQuery.ajax/
    //-----------------------------------------------------------------------
    $.ajax({                                      
      url: 'api.php',                  //the script to call to get data          
      data: "",                        //you can insert url argumnets here to pass to api.php
                                       //for example "id=5&parent=6"
      dataType: 'json',                //data format      
      success: function(data)          //on recieve of reply
      {
        var id = data[0];              //get id
        var vname = data[1];           //get name
        //--------------------------------------------------------------------
        // 3) Update html content
        //--------------------------------------------------------------------
        //*/$('#output').html("<b>id: </b>"+id+"<b> name: </b>"+vname); //Set output element html

/*
        var vals = Object.keys(data).map(function(key) {
            return data[key];
        });
*/

        //alert(vals);

        var count = Object.keys(data).length;
        console.log(count);

          var x = 0;
          $('#output').html("<b>all: </b>"+ JSON.stringify(data)); //Set output element html

          while (x <= count){
            var txt1 = "<b> sms: "+ data[x].name + "</b> <i> from: " +  data[x].ref + "</i> <br />";  // Create element with HTML


            //var txt2 = $("<i></i>").text("love ");     // Create with jQuery
            //var txt3 = document.createElement("b");    // Create with DOM
            //txt3.innerHTML = "jQuery!";
            //$("div").after(txt1, txt2, txt3);          // Insert new elements after <img>
            $("div").after(txt1);          // Insert new elements after <img>
            x++;
          }

        //recommend reading up on jquery selectors they are awesome 
        // http://api.jquery.com/category/selectors/
      } 
    });
  }); 

  </script>
  </body>
</html>
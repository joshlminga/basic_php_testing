<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>For Loop</title>
</head>

<body>

<!--- Let's first see the different between while and for loop : 
* We will use the previous while loop
-->

<?php 

//Let's see how it's works 

$count = 0 ;

while ($count <= 10) //condition to check
 { 
	
	echo "Count = {$count}  <br />" ;
	$count ++;  //increment the value so we may not get inifnity loop (loop with no end)
}


?>

<br />

<?php

for ($num=0; $num <= 10; $num++) {  //for loop syntax ... expression 1 $num = 0; expression 2 $num <= 10 (is like saying do this when $num <= 10) expression 3 $num++ increment $num
	echo "Number = {$num} <br />" ; //echo $num continuosly when the loop is true
}

?>

<?php


?>

<!--- Both loops will give you same result -->

</body>
</html>

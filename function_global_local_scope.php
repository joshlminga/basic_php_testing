<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Function Global and Local Scope</title>
</head>

<body>

<!--- Note all variables declaired withing a function can not be accessed from outside a function;
	thats why we had to return a variable so it can also be accessed from outside a function
	those variable inside a function are called local scope, and ones from outside are called global scope
-->

<?php
  echo "----------------1------------------- <br />";

	$bar = "Kayole <br />" ;  // global scope

	function foo() {

		$bar = "Saika"; // local scope
		
	}

	echo $bar; // Now lets try to echo the global variable and see then let's us call the function foo and echo the local scope variable
	$bar = foo();    // will it work ??
	echo $bar . "<br />";  // No even when we call the function it still print the global value....

	
?>

<br />

<!-- You found out the out put is the same : may be let's try the other way and see -->

<?php

echo "----------------2------------------- <br />";

	$bar1 = "Donholm <br />" ;  // global scope

	function foo1($bar1) {

		if (isset($bar1)) {  // we want to make sure "the variable is set??"

			echo "Home : {$bar1} <br />";  // we found it is set...: Then lets  proceide calling the function and echo word Home : Thika
		}

		$bar1 = "Umoja"; // local scope
		
	}

	echo $bar1 . "<br />"; // Okay our global variable will be here I axpect to see word Donholm ..
	foo1("Thika");    // okay we have called the function .... now let's output word Umoja
	echo $bar1 . "<br />"; // Did it work as we expected?? 


?>

<!--- No it still output the global variable but why?? 
simply because the variable was declaired before the function so it at as supirior to the end

-->
<br />
<?php

echo "----------------3------------------- <br />";

// lets solve this issue: By declairing a global variable 

$bar2 = "Naivasha <br />" ;  // global scope

	function foo2() {

		global $bar2;

		if (isset($bar2)) {  // we want to make sure "hey is even the variable set??"

			echo "Home : {$bar2} <br />";  // we found it is set...: Then lets  proceide calling the function and echo word Home : Naivasha
		}

		$bar2 = "Rongai"; //local scope
		 
	}

	echo "Glo : {$bar2} <br />"; // Okay our global variable will be here I axpect to see word Naivasha ..
	foo2(); 
	echo "Fun : {$bar2} <br />";    // okay we have called the function .... now let's output word Rongai
	 // It works 
	
?>
</body>
</html>

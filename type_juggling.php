<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Type Juggling</title>
</head>

<body>
	<h2>Type Juggling</h2> <br />

<?php

$count = "38"; // we know though it contain integers it is a string value
$count1 = "2 dogs" ; // thi is also a string
$tape = 3; // thi is an integer 

?>

<br />

<!--- Lets get type of each variable -->

Type : <?php echo gettype($count); // it will echo a string because varibale count is a string ?> <br />

<!-- let see what will happen if we add string count1 and integer tape -->
Answer : <?php echo $mine = $count1 += $tape; // apache will try to convet " 2 dogs" to an integer so it can be added with 3: there for it will take 2 and chop out the rest ?> <br />
Type : <?php echo gettype($mine); // it will say is an integer because it will convert string "2 dogs" to an integer ?> <br />

<?php $just = "I have" . $count1 . "and a cat" ; // php will covert all to string because our variable is populated ny string values ?> <br />
Type : <?php echo gettype($just); // it will be a string ?>

<!--- But it is not agood practise to depend on apache to do convertion for use we can perform it on our own -->
<br />

<h2>Type Casting</h2> <br />

<?php $count1 = 1000.00; ?> 
<?php settype($count1, "double"); // set type affect the value inspot: [note - specification should be in double quotes "string" / "integer" ?> <br />
To integer : <?php echo $count1;//gettype($count1); // we want to see if it was changed to integer ?> <br />

<!--- There is anothe way of doing that -->

<?php $count2 = (string) $count1 ; // we say convert integer count1 to string but assign the convertion in variable count2 and let count1 remain as integer ?> <br />
To String : <?php echo gettype($count2); // it will be a string ?> <br />
To integer : <?php echo gettype($count1); // but count1 is still an integer: because this method does not affect the variable in spot ?> <br /> 

<br />

<!--- Let's check the basic different between settype and assign method ()  -->

<?php 

$test1 = 3;
$test2 = 3;

settype($test1, "string"); //change variable test1 to string 
(string) $test2 ; //change variable test2 to string 

?>

<!--- now lets see the basic different -->

Test 1 : <?php echo gettype($test1); // it will be a string ?><br />
Test 2 : <?php echo gettype($test2); // it will be an integer because the changes did not last was just for that moment ?><br />

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ternary</title>
</head>

<body>


<!-- Ternary Operator works as IF...Else Statement -->
<!-- Syntax

	(condition) ? TRUE : FALSE;
-->
<?php
$no = 2;
$drop_on = 'Yes';
$drop_off = 'No';

?>


<?php $active = ($no >=2 && $no <3 ) ? $drop_on : $drop_off; echo $active;?>

<br />

<?php

$valid_mail = 1;
$failed = ($valid_mail > 0 ) ? 'email' : 'ID';

?>

Valid Failed : <?php echo "{$failed}"; // Answer will be Email ?>


</body>
</html>

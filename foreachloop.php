<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>For Each : Loop</title>
</head>

<body>

<!--- foreach loop, used to loop inside an array : for example we have an array with no limit: we can loop inside that array and assign values using foreach loop
-->	
<?php
// For Normal Arrays

	$age = array(14,23,44,55,69,12) ; // we have our array

	foreach ($age as $myage) {   // we have set each array on variable myage and echo it without use index position or key name: and there is no condition set
		echo "My Age is : {$myage} <br />";
	}
?>

<br />

<?php
// For Associative Arrays

	$person = array(

		'First_Name' => "Josh" , 
		'Last_Name' => "Minga", 
		'Phone_Number' => 0708548932, 
		'Nationa_ID' => "#1287453", 
		'Email_Address' => "josh@techweb-solutions.com" 

		); // we all know this is our associative array person

	foreach ($person as $label => $data) { // foreach ( associative array AS key => data which is in the key ) {}
										   // foreach ($array as $key => $value) {} 
	 // Array is already indetified ($person), each key name is assigned to $label (can be of any name not must label), then data within that key are assigned to $data (can be of any name not must data)

		$labels = ucwords(str_replace("_", " ", $label));  // We set the first Words to be in capital letter (ucwords), then we replace the underscore with space str_replace("_"," " in the key ($label) :
														   // Because we want to use key name as our out put label: and we assigne the new looking label no anothe variable labels 
		echo "{$labels} : {$data} <br />";  // see the output here
	}
?>

<br />
<!--- Consider anothe example -->

<?php

$prices = array('leaning_java' => 2000 , 'leaning_pHP' => 5000 , 'leaning_mysqli' => null , );

foreach ($prices as $service => $cost) {
	if (is_int($cost)) {

		echo "{$service} cost US$ : {$cost} <br />"; // now we echo the key name without removing the underscore or capitalise the first letter :see the results
		
	} else {

		echo "{$service} cost US$ : priceless <br />"; 

	}
	
}

?>

</body>
</html>

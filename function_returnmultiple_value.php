<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Return Multiple Values</title>
</head>

<body>
	<?php
// Note Function only return one value 
/*
* return $sum;  
*/

// Now how can we return multiple values??

function add_input($val1, $val2) {

	$sum = $val1 + $val2 ; 
	$sub = $val1 - $val2 ;
	return $sum; 
}

$myanswer = add_input(3,4);  // it will return only sum of of substraction and not both,
echo $myanswer ."<br />";
// So is ther a php value that can hold more that one variables??
// Yes Array can ....
	?>

<?php

function sum_sub($val1, $val2) {

	$sum = $val1 + $val2 ; 
	$sub = $val1 - $val2 ;
	$answer = array($sum,$sub); 

	return 	$answer = array($sum,$sub); 

	//return $answer; 
}	

$the_answer = sum_sub(3,4) ;
echo "The Sum is = {$the_answer[0]} <br />";
echo "The Sub is = {$the_answer[1]} <br />";

/*
* Using array to return value from a function is very useful because we can return alot of this..
* But there is another better way to do so..
*/
// usinging function list instead of array...

list($addition,$substraction) = sum_sub(12,6);
echo "The Sum is = {$addition} <br />";
echo "The Sub is = {$substraction} <br />";

// actually using list may come at handy because each value will be represented using simillar variable name

?>

<br />


</body>
</html>

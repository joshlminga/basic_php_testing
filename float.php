<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Float Numbers</title>
</head>
<body>

<?php 
$float = 3.15;
$float2 = 7.453679;
$Numb = 7;
?>

<br />
Float addition : <?php echo $float + $Numb;  //float works okay with integers ?> <br />
Division : <?php echo 4/3; //even when we divide we can get answer in float value ?> <br />
Round : <?php echo round($float2, 3); // we can round off a decimal value(float) ?> <br />
Celling : <?php echo ceil($float2); ?> <br />
Floor : <?php echo floor($float2); ?> <br />   

<br />
Note : True is converted to string and become 1 ; while False changes to O == empty to boolen <br />

<br />
Is 7 an integers : <?php echo is_int($Numb); // check if the value is an integer ?> <br /> 
<?php echo "Is {$float2} an integer : " . is_int($float); // check is float is an integer ?> <br />

<br />
Is 7 a float : <?php echo is_float($Numb); // check if the value is an float ?> <br /> 
<?php echo "Is {$float2} a Float : " . is_float($float); // check is float is an float ?> <br />

<br />
Is 7 a Numeric : <?php echo is_numeric($Numb); // check if the value is a Numeric ?> <br /> 
<?php echo "Is {$float2} a Numeric : " . is_numeric($float); // check is float is a Numeric?> <br />

</body>
</html>

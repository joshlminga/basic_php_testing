<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Array Function</title>
</head>

<body>

<?php 

$numbers1 = array(4,5,16,30,22,55,3); // integer array


?>
<br />
<pre>
Preview whole array : <?php print_r($numbers1);//print_readable(print_r);we use this method to view what is in array not for user but for us ?> <br />
</pre>

Count : <?php echo count($numbers1); // we can count values that are in the array ?> <br />
Max Value : <?php echo max($numbers1); // we can get the max value ?> <br />
Min Value : <?php echo min($numbers1); // we can get the minimum value ?> <br />
<br />

<pre>
Sort Ascending : <?php sort($numbers1); print_r($numbers1); // sort ascending ?> <br />
Sort Descending : <?php rsort($numbers1); print_r($numbers1); // sort descending ?> <br />
</pre>


<pre>
Get Array Lenght : <?php echo count($numbers1);?>
</pre>
<!--- the sort & reverse sort (rsort) are desractive function; means our unsorted version of array doesn't exist any more
	so our array numbers1 is now sorted descending -->

<br />
<!--- We can change an array to a string using implode function -->

Implode : <?php echo $num_string = implode("|", $numbers1); //we have use | as a string separator you can use *,# or any character but it should in "" ?> <br />

<!--- Oposite of implode is explode; means when it found a character it will take it as a saperator and it will remove it -->
<!--- Let's first echo string num_string to see what is on it -->

<pre>
Num_String : <?php echo $num_string; // what is inside num_string ?> <br />
</pre>

<!--- Now lets see how can we remove the saporator and make our string an array again -->

<pre>
Explode : <?php print_r(explode("|", $num_string)); // I tell apache remove separator | in string num_string; and it will return to be an array ?> <br />
</pre>

<!--- It is usefull to know explode and implode functions ; they can be useful when working with comma separated values 
	e.g. name,address,phone number, e.t.c : you can tell apache okay "the saparator is "," remove it and sore the value in an array the
	first value to array 0 e.t.c -->

<br />

<?php $check1 = array(14,75,6,3,2,35,13); ?>

<!-- Also we can check if the value is inside a array or not: we look by using needle(the value) and haystack(the array) -->
Is 19 inside array numbers1? : <?php echo in_array($check1, $numbers1); // is 19 in array numbers? : it will return true(1) or False(0) ?> <br />
Is 30 inside array numbets1? : <?php echo in_array(30, $numbers1); // is 30 in array numbers? : it will return true(1) or False(0) ?> <br /> 

SUbstring 14S03ADIT005 TO GET CAMPUS ID = <?php $id = '14S03ADIT005'; $CAMPUS = mb_substr($id,3,2); echo "{$CAMPUS}";?>

<?php
$input = array("a" => 1, "b" => 1, "c" => 2);
$flipped = array_flip($input);


?>
FLIPPED : <?= print_r($flipped);?>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Logical If</title>
</head>

<body>

	<?php 

	// Used to check if the value tested are true ; and if they are true do what?

	$a = 4;
	$b = 5;

	if ($a > $b)	{
		echo "A is grater that B"; // Is like say "hey check for if is 4 is grater than 5 and if true echo this:"
	}
	elseif ($a < $b) {
		echo "A is less than B"; // Is saying "Hey if 4 is not greater than 5 then check if 4 is less than 5; and if true echo this"
	}

	else {

		echo "A is equal to B"; // If the all test are not correct then echo this
	}

	?>

</body>
</html>

<?php

//header("HTTP/1.1/ 404 Not Found");

/*
header("Location: home.php");
exit;

//The best way is to use a function


*/

function redirec_to($location){	
	header("Location: ". $location);  //Append the value
	exit;
}


$go_to = "basic.html";
redirec_to($go_to);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HTML HEADERS</title>
</head>

<body>

<!-- Example 

GET /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Connection: Keep-Alive

-->

<?php

	//header(string);

	//Using header we can tell HTML which content type we want the documennt to be

	// --> header("Content-Type: application/pdf");


	// Header must come first before any thing else... any other HTML Elements or a space unless using OUTPUT BUFFER
	// Look Example at the top

?>


</body>
</html>

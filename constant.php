<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Constatnt</title>
</head>

<body>

<!--- There are variable; we know variables can be assigned and reassigned
	* But what if we need a constant that can not be interfeared??
	*Below is how we define the constant 
-->

<?php

	define("PIE", 3.14); // here we have define constant PIE as 3.14;

	/* constant does not need dollar sign($) to be declaire of introduced; let see how to call the constant */

	echo PIE; // no use of dollar sign($) as in variables

	/* we can not even re define the constant neither Arthimetic Operation on it : if you uncomment the following code you will get an error
	* because constant can not be added with number or re defined
	*/ 

	/* define("PIE", 22/7); // redefine a constant: but it wont work 
	 * echo PIE; // error will be shown

	 *Let's add value to it then 

	 * echo PIE + 3 ; // still you will get an error 

	 */

	// CONSTANT HAS TO BE IN CAPITAL LETTERS //

?>

</body>
</html>

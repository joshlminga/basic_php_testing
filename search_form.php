
<form action="http://localhost/Nairobi_Nanny/search/personale" method="post" accept-charset="utf-8" class="bootstrap-frm">    

	<h1 class="classy">Search for Personale </h1>    
	<label>        
		<span>Type of Personale:</span>        

		<select name="location">        
			<option value="18">House Help</option>        
			<option value="19">Cook</option>        
			<option value="20">Driver</option>        
			<option value="21">Gardener</option>        
		</select>    
	</label>         

	<label> 
		<span>Age Between:</span>

		<select name="age_range">        
		    <option value="18-24">18 - 24</option>        
		    <option value="25-30">25 - 30</option>        
		    <option value="21-36">21 - 36</option>        
		    <option value="37-46">37 - 46</option>        
		</select>    
	</label>         

	<label>        
		<span>Gender:</span>

		<select name="gender_range">        
			<option value="14">Male</option>        
			<option value="15">Female</option>        
		</select>    
	</label>         

	<label>        
		<span>Desired Salary:</span>

		<select name="salary_range">        
			<option value="4001-6000">4001 - 6000 KSH</option>        
			<option value="6001-8000">6001 - 8000 KSH</option>        
			<option value="8001-10000">8001 - 10000 KSH</option>        
			<option value="10001">over 10000 KSH</option>        
		</select>    
	</label>         

	<label>        
		<span>&nbsp;</span>         
		<input type="submit" class="button" onmouseover="JavaScript:this.style.cursor='pointer'" value="Search" />    
	</label>    

</form>

<!--
 <input type="text"	name="company_location" id="typeahead" value="" data-provide="typeahead" data-items="4" 
 		data-source='["Baringo","Bomet","Bungoma","Busia","Elgeyo/Marakwet",
 		"Embu","Garissa","Homa Bay","Isiolo","Kajiado","Kakamega",
 		"Kericho","Kiambu","Kilifi","Kirinyaga","Kisii","Kisumu",
 		"Kitui","Kwale","Laikipia","Lamu","Machakos","Makueni",
 		"Mandera","Marsabit","Meru","Migori","Mombasa","Murang’a",
 		"Nairobi","Nakuru","Nandi","Narok","Nyamira","Nyandarua",
 		"Nyeri","Samburu","Siaya","Taita Taveta","Tana River",
 		"Tharaka Nithi","Trans Nzoia","Turkana","Uasin Gishu","Vihiga",
 		"Wajir","West Pokot"]'>

-->
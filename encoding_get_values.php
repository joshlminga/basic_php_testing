<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Encoding GET Values</title>
</head>

<body>

<!--- There are special reserved characters that if we use the with our url we migth end up with an error
	- ! @ # $ % & * { } | 
	- the only way if we must use them is to convert them to respectively hexadecimal value send them through url and decode them
	-->

<?php

/* using function urlencode(); is going to let go letters,numbers,underscore, and dash unchanged
* but special characters are going to become %(hexadecimal digits) e.g. %28 
* space become +
** unless when using rawurlencode($company);  will convert space to 20% ... click page source to view it
*/


/* Which encode type to use and where??

=> Using rawurlencode
	* when you want to echo a path or anything before ?
	* all space before ? must be as %20

=> Using urlencode
	* after the ? tag
	* when dealing with string is better to use space as + 

********** rawurlencode is more advisible to us *************


/*
let's go back to firstpage.php and try this
*/

// - - - - - - - - - - - - - - - -  - - - - -  - - - - -  - -  - - - - - - - - - - - - - - - - -- - - - - - - -- - - - - - -- - - - -- - 

?>

</body>
</html>

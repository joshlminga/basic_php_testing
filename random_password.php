<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Random Password</title>
</head>

<body>

<?php
function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789-){=|,?/}[&]<>^%$#@!~`";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

echo "Password : " .randomPassword();
?>

<br />
<?php $image_xtn = 'png'; $candidate_image = 'asset/img/candidate/'. substr(md5(time()), 0,10) . '.' .$image_xtn; ?>
<span>Image : <?php echo "$candidate_image"; ?></span>
<?php /*delete afile ->*/ //unlink(filename); ?>

</body>
</html>

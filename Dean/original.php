<?php
//session_start();
include "classes/vars.php";
include "classes/sessions.php";

if ($logged == 0) {
header("Location: /order/login.php");
}


	$THIS_VERSION = '3.4';        // Version of this file

	//require_once 'ubr_ini.php';
	//require_once 'ubr_lib.php';

	//if($_INI['php_error_reporting']){ error_reporting(E_ALL); }

	//Set config file
	if($_INI['multi_configs_enabled']){
		//////////////////////////////////////////////////////////////////////////////
		//	ATTENTION
		//
		//	Put your multi config file code here. eg
		//
		//	if($_SESSION['user_name'] == 'TOM'){ $config_file = 'tom_config.php'; }
		//	if($_COOKIE['user_name'] == 'TOM'){ $config_file = 'tom_config.php'; }
		//////////////////////////////////////////////////////////////////////////////
	}
	else{ $config_file = $_INI['default_config']; }

	// Load config file
	require_once $config_file;

	//debug($_CONFIG['config_file_name'], $_CONFIG); exit();

	if($_INI['debug_php']){ phpinfo(); exit(); }
	elseif($_INI['debug_config']){ debug($_CONFIG['config_file_name'], $_CONFIG); exit(); }
	elseif(isset($_GET['about'])){
		kak("<u><b>UBER UPLOADER FILE UPLOAD</b></u><br>UBER UPLOADER VERSION =  <b>" . $_INI['uber_version'] . "</b><br>UBR_FILE_UPLOAD = <b>" . $THIS_VERSION . "</b><br>\n", 1, __LINE__, $_INI['path_to_css_file']);
	}



add_action('woo_title','insert_custom_title');

function insert_custom_title(){
$title = "Upload files to order";
return $title;
}

include "header.php";
/*echo "<pre>";
print_r($_COOKIE);
echo "</pre>";*/
?>
<div class="container">
<p>Upload files to your order <a href="view.php?order_id=<?=$_COOKIE["order_idx"];?>"><?=$_COOKIE["order_idx"];?></a>. The system supports many files upload at a time. Click on “CHOOOSE FILE” and open the file, repeat the same to add subsequent files until all your files are listed. Click on UPLOAD button and you will view the Upload in progress as the files are being uploaded. Upon completion, click next to proceed to your order page.</p>
<p><i> NB: The system will not allow you to upload duplicated files if you have such files rename it to have a unique name, to remove a file from the list click on the X button at the right hand side of the file name.</i></p>
		<div id="main_container">
			<?php if($_INI['debug_ajax']){ ?><div id='ubr_debug'></div><?php } ?>
			<div id="ubr_alert"></div>

			<!-- Progress Bar -->
			<div id="progress_bar_container">
				<div id="upload_stats_toggle">&nbsp;</div>
				<div id="progress_bar_background">
					<div id="progress_bar"></div>
				</div>
				<div id="percent_complete">&nbsp;</div>
			</div>

			<br clear="all">

			<!-- Upload Stats -->
			<?php if($_CONFIG['show_files_uploaded'] || $_CONFIG['show_current_position'] || $_CONFIG['show_elapsed_time'] || $_CONFIG['show_est_time_left'] || $_CONFIG['show_est_speed']){ ?>
				<div id="upload_stats_container">
					<?php if($_CONFIG['show_files_uploaded']){ ?>
					<div class='upload_stats_label'>&nbsp;Files Uploaded:</div>
					<div class='upload_stats_data'><span id="files_uploaded">0</span> of <span id="total_uploads">0</span></div>
					<?php }if($_CONFIG['show_current_position']){ ?>
					<div class='upload_stats_label'>&nbsp;Current Position:</div>
					<div class='upload_stats_data'><span id="current_position">0</span> / <span id="total_kbytes">0</span> KBytes</div>
					<?php }if($_INI['cgi_upload_hook'] && $_CONFIG['show_current_file']){ ?>
					<div class='upload_stats_label'>&nbsp;Current File Uploading:</div>
					<div class='upload_stats_data'><span id="current_file"></span></div>
					<?php }if($_CONFIG['show_elapsed_time']){ ?>
					<div class='upload_stats_label'>&nbsp;Elapsed Time:</div>
					<div class='upload_stats_data'><span id="elapsed_time">0</span></div>
					<?php }if($_CONFIG['show_est_time_left']){ ?>
					<div class='upload_stats_label'>&nbsp;Est Time Left:</div>
					<div class='upload_stats_data'><span id="est_time_left">0</span></div>
					<?php }if($_CONFIG['show_est_speed']){ ?>
					<div class='upload_stats_label'>&nbsp;Est Speed:</div>
					<div class='upload_stats_data'><span id="est_speed">0</span> KB/s.</div>
					<?php } ?>
				</div>
				<br clear="all">
			<?php } ?>

			<!-- Container for upload iframe -->
			<div id="upload_container"></div>

			<!-- Start Upload Form -->
			<form id="ubr_upload_form" name="ubr_upload_form" method="post" enctype="multipart/form-data" action="#" onSubmit="return UberUpload.linkUpload();">
				<noscript><span class="ubrError">ERROR</span>: Javascript must be enabled to use Uber-Uploader.<br><br></noscript>
				<div id="file_picker_container"></div>
				<div id="upload_slots_container"></div>
				<div id="upload_form_values_container">
				<!-- Add Your Form Values Here -->
				</div>
				<div id="upload_buttons_container"><input type="button" id="reset_button" name="reset_button" value="Reset">&nbsp;&nbsp;&nbsp;<input type="submit" id="upload_button" name="upload_button" value="Upload"></div>
			</form>
		</div>
		<br clear="all">

</div>
<?php

include "footer.php"; 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Break</title>
</head>

<body>

	<!--- Break : Is an oposite of continue
	* e.g. Is like Hollywood director in the middle of audition say "Hey stop - You are the one, you get the Job";
	* and the audition ends here
	  -->

<?php 


for ($num=0; $num <= 10; $num++)
 { 
	
	if ($num == 5) {		
		
		Break; // means if count == 5 : end the operation here
	}

		echo $num. " , " ; // Lets out put the values and see what happened 

		//ooops! note: that after it's echo 0,1,2,3,4, it stops

		/* so What does BREAK realy do??
		* Break stops the loop ; let's say we have 2000 values and we got our data at 5th value: should we loop till the 2000th 
		* No! that's why BREAK comes at handy
		*/

}

?>

<br />

<?php

for ($i=0; $i <= 5; $i++) { 
	if ($i % 2 == 0) { Continue; }  // we say hey if is even number skip

	for ($k=0; $k <= 5; $k++) { 

		if ($k == 3) { Break(2); } // hey if you find 3 break { BREAK as Continue; has a default value of 1 (Break (1)) - so if we add something like Break(2)  }
									/*
									* We say Break , and do the same in 2 forloops
									*/
		echo $i  ."-" . $k . "<br />";

		}


}

?>

</body>
</html>

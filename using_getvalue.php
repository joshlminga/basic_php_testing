<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GET VALUE</title>
</head>

<body>

	<!--- as we have see in the last example the url/link reguest is sent throgh GET method.. now how to get those request ?? -->

	<!-- As our last example (firstpage.php) introduced a method of using id to submit request: lets look more on it
	* as we know we can use
	- somepage.php?op=12  to get the same code but with additional scripts..
	- gotopage.php?cartegory=7&page=12  using uperand to get more return values
	-->

<?php

//in PHP when all request is sent: it will put all in associative array which will be know as a super global variable
/*
Now we know about global values (remember in global & scope value??)... now here is super global 
* There are many super globe; but the one we will look into now is the one that all the value sent from url are assigned to
* That super global variable is _GET ($_GET) : it is php reserved variable hence can't be used as custom variable

- - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - -  -- - - - - -- - - -  -- - - 

Now : on our last example firstpage.php we sent a value through a super global $_GET to request for secondpage.php?id=2 ...

so id=2 is the main value needed inside the secondpage.php....
look at the address bar and you will see : http://localhost/PHP_TESTING/secondpage.php?id=2 
the main part is : secondpage.php?id=2 
that means id is set to 2

- - - - - - - - - - - - - - --  - - - -- - - - - - - - - -- - - - - - - - - - - - - - - - - -- - - - -- - -

Since super global $_GET is an associative array: and it works with links
Now let's go to secondpage.php and try to print the associative array sent from firstpage.php using url/link that was assigned to super global _GET

=> Go to secondpage.php
*/
?>

</body>
</html>
